#!/usr/bin/env python
# coding: utf-8

# In[1]:


from pymongo import MongoClient
from pprint import pprint
import re
import csv
import pandas as pd
import numpy as np
from bson.objectid import ObjectId
import time
from bson.json_util import dumps
import json


# In[2]:


user = "root"
password = ""

client = MongoClient("mongodb://", username=user, password=password)

db = client.fbot


# In[3]:


# name = "Sữa bột Anlene Movepro Hương Vanilla hộp 440g (Từ 19 đến 45 tuổi)"
# # pprint(flashsale.find_one())

# pipeline1 = [
#     {'$lookup':
#          {'from': 'flash_deal_info',
#           'localField': "name",
#           'foreignField': "title",
#           'as': 'FlashDealSale'}},
#     {'$match':
#         {'name': name}},
# ]

# print(list(db.items.aggregate(pipeline1)))

# for doc in db.items.aggregate(pipeline1):
#     pprint(doc)


# In[4]:


def get_all_phone():
    pipeline = [
        {'$match':
             {'$or':[
                 {'category': "Cell Phones"},
                 {'category': "Cell Phones & Tablets"},
             ]}
        },
    ]
    items = db.items.aggregate(pipeline)
    
    iitems = []
    for item in items:
        iitems.append(item)

    return iitems

tmps = get_all_phone()
print(len(tmps))


# In[5]:


def get_products_by_name_and_colors(name="OPPO F9", colors=["vàng", "Nhiều màu"]):
    '''
    Lấy sản phẩm dựa trên tên_sản_phẩm  và màu_sắc yêu cầu của người dùng
    
    $name là tên của sản phầm cần tìm
    $colors là 1 list màu_sắc cần tìm
    '''
#     {'$regex': {'$in': [re.compile(color, re.I) for color in colors]}}
    
    pattern = re.compile(name, re.I)
    pattern_colors = "|".join(color for color in colors)
    pipeline = [
        {'$match':
             {'$and':[
                 {'name': {'$regex': pattern}},
                 {'$or':[
                     {'category': "Cell Phones"},
                     {'category': "Cell Phones & Tablets"},
                 ]},
                 {'specs.Màu': {'$regex': pattern_colors, '$options': 'i'}},
             ]}},
        {'$lookup':
             {'from': 'item_prices',
              'localField': 'url', 
              'foreignField': 'url', 
              'as': 'item_prices'}},
        {'$unwind': '$item_prices'},
        {'$addFields': {'prices': {'$arrayElemAt': ['$item_prices.price_history', 0]}}},
        {'$project':{
            'name': 1,
            'specs': 1,
            'prices.final_price': 1,
            'prices.original_price': 1,
            'prices.discount_rate': 1,
            'marketplace': 1,
        }}
    ]
    
    items = db.items.aggregate(pipeline)
    
    iitems = []
    for item in items:
        iitems.append(item)
    
    return iitems

tmps = get_products_by_name_and_colors(name="OPPO F9", colors=["vàng", "Nhiều màu"])
print(len(tmps))
tmps


# In[6]:


def get_products_by_name(name="OPPO F9"):
    '''
    Lấy giá của sản phẩm (giá gốc, giá khuyến mại) dựa trên tên sản phẩm
    '''
    
    pattern = re.compile(name, re.I)
    pipeline = [
        {'$match':
             {'$and':[
                 {'name': {'$regex': pattern}},
                 {'$or':[
                     {'category': "Cell Phones"},
                     {'category': "Cell Phones & Tablets"},
                 ]},
             ]}},
        {'$lookup':
             {'from' : 'item_prices',
              'localField' : 'url',
              'foreignField' : 'url',
              'as' : 'item_prices'}},
        {'$unwind': '$item_prices'},
        {'$addFields': {'prices': {'$arrayElemAt': ['$item_prices.price_history', 0]}}},
        {'$project':{
            'name': 1,
            'specs': 1,
            'prices.final_price': 1,
            'prices.original_price': 1,
            'prices.discount_rate': 1,
            'marketplace': 1,
        }}
    ]
    items = db.items.aggregate(pipeline)
    
    iitems = []
    for item in items:
        iitems.append(item)

#     schema = [["name", "url", "originalprices", "finalprices"]]
    
#     with open('./prices_product_by_name.csv', 'a') as f:
#         writer = csv.writer(f, dialect='excel', delimiter="\t")
#         writer.writerows(schema)
        
#         for item in items:
            
#             original_prices = []
#             final_prices = []
#             len_item_prices = len(item["item_prices"])
#             for i in range(len_item_prices):
#                 final_prices.append(item["item_prices"][i]["price_history"][0]["final_price"])
#                 original_prices.append(item["item_prices"][i]["price_history"][0]["original_price"])
            
#             original_prices = list(set([price for price in original_prices if len(price) > 4]))
#             final_prices = list(set([price for price in final_prices if len(price) > 4]))
            
# #             print(item["name"], ' - ', item["url"])
                
#             row = [[item["name"], item["url"], original_prices, final_prices]]
            
#             writer.writerows(row)
    
#     f.close()

    return iitems

tmps = get_products_by_name(name="OPPO F9")
print(len(tmps))
tmps


# In[7]:


def get_products_by_price(start=None, end=None, option=True):
    """
    Hàm mục đích tìm kiếm điện thoại dựa vào giá:
    
    + Nếu có ">=", "<=" hoặc cả 2 thì option=True
        Nếu >= 3000000VND thì end = 3000000VND
        Nếu <= 3000000VND thì start = 3000000VND
        Nếu 2000000VND <= x <= 3000000VND thì start = 2000000VND, end = 30000000VND
    
    + Ngược lại option = False, tức là 'khoảng', ví dụ: "Tôi cần tìm điện thoại khoảng 300000VND"
        Khi đó sẽ xử lý lấy các sản phẩm (khoảng - 1 triệu/trăm) <= x <= (khoảng - 1 triệu/trăm)
        
    Note: chưa code cho khoảng 
    
    """
    
    if (start == None) and (end == None):
        print("Yêu cầu về giá không hợp lệ !")
        print(0)
    elif (start != None) and (end == None):
        pipeline = [
            {'$lookup':
                 {'from': 'item_prices', 
                  'localField': 'url',
                  'foreignField': 'url',
                  'as': 'item_prices'}},
            {'$unwind': '$item_prices'},
            {'$addFields': {'prices': {'$arrayElemAt': ['$item_prices.price_history', 0]}}},
            {'$match':
                 {'$and':[
                     {'prices.final_price': {'$gte': '10000VND', '$lte':start}},
                     {'$or':[
                         {'category': 'Cell Phones'},
                         {'category': 'Cell Phones & Tablets'}
                     ]},
                 ]}},
            {'$project':{
                'name': 1,
                'specs': 1,
                'prices.final_price': 1,
                'prices.original_price': 1,
                'prices.discount_rate': 1,
                'marketplace': 1,
            }}
        ]
    elif (start != None) and (end != None):
        pipeline = [
            {'$lookup':
                 {'from': 'item_prices', 
                  'localField': 'name',
                  'foreignField': 'title',
                  'as': 'item_prices'}},
            {'$unwind': '$item_prices'},
            {'$addFields': {'prices': {'$arrayElemAt': ['$item_prices.price_history', 0]}}},
            {'$match':
                 {'$and':[
                     {'prices.final_price': {'$gte': start, '$lte': end}},
                     {'$or':[
                         {'category': 'Cell Phones'},
                         {'category': 'Cell Phones & Tablets'}
                     ]},
                 ]}},
            {'$project':{
                'name': 1,
                'specs': 1,
                'prices.final_price': 1,
                'prices.original_price': 1,
                'prices.discount_rate': 1,
                'marketplace': 1,
            }}
        ]
    elif (start == None) and (end != None):
        pipeline = [
            {'$lookup':
                 {'from': 'item_prices', 
                  'localField': 'name',
                  'foreignField': 'title',
                  'as': 'item_prices'}},
            {'$unwind': '$item_prices'},
            {'$addFields': {'prices': {'$arrayElemAt': ['$item_prices.price_history', 0]}}},
            {'$match':
                 {'$and':[
                     {'prices.final_price': {'$gte': end}},
                     {'$or':[
                         {'category': 'Cell Phones'},
                         {'category': 'Cell Phones & Tablets'}
                     ]},
                 ]}},
            {'$project':{
                'name': 1,
                'specs': 1,
                'prices.final_price': 1,
                'prices.original_price': 1,
                'prices.discount_rate': 1,
                'marketplace': 1,
            }}
        ]
        
    
    items = db.items.aggregate(pipeline)
    
    iitems = []
    for item in items:
        iitems.append(item)
    
    return iitems


tmps = get_products_by_price(start='2000000VND')
print(len(tmps))
tmps


# In[8]:


def get_products_by_name_and_memories(name="OPPO F9", memories=["32GB", "64GB"]):
    '''
    Lấy sản_phẩm dựa trên tên_sản_phẩm và bộ nhớ
    
    $name : tên sản phầm cần tìm
    $memories : danh sách các bộ nhớ mong muốn
    '''
    
    pattern = re.compile(name, re.I)
    pattern_memories = "|".join(memory for memory in memories)
    pipeline = [
        {'$match':
             {'$and':[
                 {'name': {'$regex': pattern}},
                 {'$or':[
                     {'category': "Cell Phones"},
                     {'category': "Cell Phones & Tablets"},
                 ]},
                 {'specs.Bộ nhớ trong': {'$regex': pattern_memories, '$options': 'i'}},
             ]}},
        {'$lookup':
             {'from': 'item_prices',
              'localField': 'url', 
              'foreignField': 'url', 
              'as': 'item_prices'}},
        {'$unwind': '$item_prices'},
        {'$addFields': {'prices': {'$arrayElemAt': ['$item_prices.price_history', 0]}}},
        {'$project':{
            'name': 1,
            'specs': 1,
            'prices.final_price': 1,
            'prices.original_price': 1,
            'prices.discount_rate': 1,
            'marketplace': 1,
        }}
    ]
    
    items = db.items.aggregate(pipeline)
    
    iitems = []
    for item in items:
        iitems.append(item)
    
    return iitems

tmps = get_products_by_name_and_memories(name="OPPO F9", memories=["32GB", "64GB"])
print(len(tmps))
tmps


# In[9]:


# def get_images_by_name(name="OPPO F9"):
#     '''
#     Lấy được hình ảnh của tên_sản_phẩm đang cần
    
#     $name : tên_sản_phẩm cần tìm
#     '''
    
    


# In[10]:


# import urllib.request

# urllib.request.urlretrieve("https://cf.shopee.vn/file/5ef2751f74ad5bff52a0d30ef7f19db1", "local-filename.jpg")


# In[11]:


# from PIL import Image
# import requests
# from io import BytesIO

# url = "https://cf.shopee.vn/file/5ef2751f74ad5bff52a0d30ef7f19db1"
# response = requests.get(url)
# img = Image.open(BytesIO(response.content), 'r')
# img.show()


# In[ ]:




